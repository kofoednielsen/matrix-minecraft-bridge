import simplematrixbotlib as botlib
from mctools import RCONClient
import os
import sh
import asyncio
from async_tail import atail

def getenv(var):
    result = os.getenv(var)
    if not result or result == "":
        print(f"Missing environment variable {var}")
        exit()
    return result

MATRIX_USERNAME = getenv("MATRIX_USERNAME")
MATRIX_PASSWORD = getenv("MATRIX_PASSWORD")
MATRIX_HOMESERVER = getenv("MATRIX_HOMESERVER")
MATRIX_ROOM_ID = getenv("MATRIX_ROOM_ID")
MINECRAFT_SERVER = getenv("MINECRAFT_SERVER")
MINECRAFT_RCON_PASSWORD = getenv("MINECRAFT_RCON_PASSWORD")
MINECRAFT_LOG_FILTER = getenv("MINECRAFT_LOG_FILTER")

creds = botlib.Creds(MATRIX_HOMESERVER, MATRIX_USERNAME, MATRIX_PASSWORD)
bot = botlib.Bot(creds)

async def minecraft_say(message):
    try:
        sender = (await bot.async_client.get_displayname(message.sender)).displayname
        rcon = RCONClient(MINECRAFT_SERVER)
        rcon.login(MINECRAFT_RCON_PASSWORD)
        message = (f"say [{sender}] {message.body}").replace("@", "\@")
        rcon.command(message)
    except Exception as e:
        print(f"Failed to print message in minecraft. Is the minecraft server online?: Error: {e}")


async def forward_logs():
    # runs forever
    async for line, file in atail("/logs/latest.log"):
        if MINECRAFT_LOG_FILTER in line and "[Rcon]" not in line:
            message = line.split(MINECRAFT_LOG_FILTER)[1].replace('\n','').replace('[Not Secure] ','')
            print(f"From MINECRAFT: {message}")
            await bot.api.send_text_message(MATRIX_ROOM_ID, message)


@bot.listener.on_message_event
async def message(room, message):
    match = botlib.MessageMatch(room, message, bot, '')
    if match.is_not_from_this_bot() and room.room_id == MATRIX_ROOM_ID:
        print(f"From MATRIX: '{message}'")
        await minecraft_say(message)

async def main():
  await asyncio.gather(bot.main(), forward_logs())

asyncio.run(main())
